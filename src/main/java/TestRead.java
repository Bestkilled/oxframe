
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.logging.Level;
import java.util.logging.Logger;


public class TestRead {
    public static void main(String[] args) throws FileNotFoundException {
        File file = null;
        FileInputStream fis = null;
        ObjectInputStream ois=null;
        Player x=null;
        Player o=null;
        try{
           file = new File("ox.bin");
           fis=new FileInputStream(file); 
           ois=new ObjectInputStream(fis);
           x =(Player)ois.readObject();
           o =(Player)ois.readObject();
           ois.close();
           fis.close();
        }catch (FileNotFoundException ex) {
            Logger.getLogger(TestOb.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(TestRead.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(TestRead.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
    }
}
